import javafx.geometry.Pos;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;


public class IMazeImpl extends UnicastRemoteObject implements  IMaze {
    private int height,width;
    private MazeObject [][] grid;
    private int agentCount;
    private int uniqueID;


    public IMazeImpl() throws  RemoteException{
        super();
        this.uniqueID = 0;
        this.agentCount = 0;
    }

    @Override
    public void create(int height, int width) throws RemoteException {
        this.width = width;
        this.height = height;
        grid = new MazeObject[height][width];
    }

    @Override
    public MazeObject getObject(Position position) throws RemoteException {
        return grid[position.getY()][position.getX()];
    }

    @Override
    public boolean createObject(Position position, MazeObjectType type) throws RemoteException {
        if(isEmpty(position)){
            if(type == MazeObjectType.AGENT){
                MazeObject maze = new Agent(position, this.uniqueID); //FIX IT
                grid[position.getY()][position.getX()] = maze;
                agentCount ++;
                uniqueID++;
            }

            else {
                MazeObject maze = new MazeObject(position, type);
                grid[position.getY()][position.getX()] = maze;
            }
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public boolean deleteObject(Position position) throws RemoteException {
        if(!isEmpty(position)){
            if(grid[position.getY()][position.getX()].getType() == MazeObjectType.AGENT)
                    agentCount--;

            grid[position.getY()][position.getX()] = null;
            return true;
        }
        return  false;
    }

    @Override
    public Agent[] getAgents() throws RemoteException {
        Agent[] agents = new Agent[agentCount];
        int pos = 0;
        for(int i = 0 ; i < height ; i++){
            for(int j = 0 ; j < width ; j++){
                Position current = new Position(j,i);
                if( !isEmpty(current) && grid[i][j].getType() == MazeObjectType.AGENT){
                    agents[pos] = (Agent) grid[i][j];
                    pos++;
                }
            }
        }
        return  agents;
    }

    @Override
    public boolean moveAgent(int id, Position position) throws RemoteException {
        Agent [] agents = getAgents();
        for(int i = 0 ; i < agents.length ; i++){
            if(agents[i].getId() == id){
                if(agents[i].getPosition().distance(position) == 1){
                    MazeObject target = getObject(position);
                    Position old = agents[i].getPosition();

                    if(target == null){
                        agents[i].setPosition(position);
                        grid[position.getY()][position.getX()] = agents[i];
                        grid[old.getY()][old.getX()] = null;
                        return true;
                    }
                    else if (target.getType() == MazeObjectType.HOLE){
                        agents[i] = null;
                        grid[old.getY()][old.getX()] = null;
                        agentCount--;
                        return true;
                    }
                    else if (target.getType() == MazeObjectType.WALL || target.getType() == MazeObjectType.AGENT){
                        return false;
                    }
                    else if (target.getType() == MazeObjectType.GOLD){
                        agents[i].setPosition(position);
                        agents[i].setCollectedGold(agents[i].getCollectedGold() + 1);
                        grid[position.getY()][position.getX()] = agents[i];
                        grid[old.getY()][old.getX()] = null;
                        return true;
                    }
                }
                else{
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public String print() throws RemoteException {
        StringBuilder string = new StringBuilder();

        string.append("+");
        for(int i= 0 ; i < width ; i++)
            string.append("-");
        string.append("+\n");

        for(int i = 0 ; i < height ; i++){
            string.append("|");
            for (int j = 0 ; j < width ; j++){
                Position current = new Position(j,i);
                if(isEmpty(current)){
                    string.append(" ");
                }
                else{
                    string.append(grid[i][j].toString());
                }

            }
            string.append("|\n");
        }

        string.append("+");
        for(int i= 0 ; i < width ; i++)
            string.append("-");
        string.append("+");

        return string.toString();
    }

    public boolean isEmpty(Position position) throws  RemoteException{
        if(grid[position.getY()][position.getX()] == null)
            return  true;
        return false;
    }

    @Override
    public String toString() {
        return "IMazeImpl{" +
                "height=" + height +
                ", width=" + width +
                ", grid=" + Arrays.toString(grid) +
                ", agentCount=" + agentCount +
                '}';
    }
}
