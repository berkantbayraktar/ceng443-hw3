import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class IMazeHubImpl extends UnicastRemoteObject implements  IMazeHub {
    private List<IMaze> mazes;

    public IMazeHubImpl() throws  RemoteException {
        super();
        this.mazes = new ArrayList<>();
    }

    @Override
    public void createMaze(int width, int height) throws RemoteException {
        IMaze maze = new IMazeImpl();
        maze.create(height,width);
        mazes.add(maze);
    }

    @Override
    public IMaze getMaze(int index) throws RemoteException {
        if(0 <= index && index < mazes.size()){
            return mazes.get(index);
        }
        else{
            return null;
        }
    }

    @Override
    public boolean removeMaze(int index) throws RemoteException {
        if(0 <= index && index < mazes.size()){
            mazes.remove(index);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "IMazeHubImpl{" +
                "mazes=" + mazes +
                '}';
    }
}
