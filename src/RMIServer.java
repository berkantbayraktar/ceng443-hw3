import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class RMIServer {
    public static void main(String[] args) throws MalformedURLException, RemoteException {
        IMazeHub server  = new IMazeHubImpl();
        Naming.rebind("mazehubservice",server);
    }
}
