import javafx.geometry.Pos;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;



public class RMIClient {
    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        IMazeHub server  = (IMazeHub) Naming.lookup("rmi://127.0.0.1/mazehubservice");
        IMaze selectedMaze = null;
        Position position = null;
        Scanner scanner = new Scanner(System.in);
        ParsedInput parsedInput = null;
        String input;
        while( true ) {
            input = scanner.nextLine();
            try {
                parsedInput = ParsedInput.parse(input);
            }
            catch (Exception ex) {
                parsedInput = null;
            }
            if ( parsedInput == null ) {
                System.out.println("Wrong input format. Try again.");
                continue;
            }
            switch(parsedInput.getType()) {
                case CREATE_MAZE:
                    server.createMaze((int)parsedInput.getArgs()[0],(int)parsedInput.getArgs()[1]);
                    break;
                case DELETE_MAZE:
                    boolean isRemoved = server.removeMaze((int)parsedInput.getArgs()[0]);
                    if(isRemoved)
                        System.out.println("Operation Success.");
                    else
                        System.out.println("Operation Failed.");
                    break;
                case SELECT_MAZE:
                    selectedMaze = server.getMaze((int)parsedInput.getArgs()[0]);
                    if(selectedMaze == null)
                        System.out.println("Operation Failed.");
                    else
                        System.out.println("Operation Success.");
                    break;
                case PRINT_MAZE:
                    String s = selectedMaze.print(); // FIX
                    System.out.println(s);
                    break;
                case CREATE_OBJECT:
                    position = new Position((int)parsedInput.getArgs()[0],(int)parsedInput.getArgs()[1]);
                    boolean isCreated = selectedMaze.createObject(position,(MazeObjectType)parsedInput.getArgs()[2]);
                    if(isCreated)
                        System.out.println("Operation Success.");
                    else
                        System.out.println("Operation Failed.");
                    break;
                case DELETE_OBJECT:
                    position = new Position((int) parsedInput.getArgs()[0], (int) parsedInput.getArgs()[1]);
                    boolean isDeleted = selectedMaze.deleteObject(position);
                    if(isDeleted)
                        System.out.println("Operation Success.");
                    else
                        System.out.println("Operation Failed.");
                    break;
                case LIST_AGENTS:
                    Agent [] agents = selectedMaze.getAgents(); // FIX
                    for(int i = 0 ; i < agents.length ; i++){
                        System.out.println("Agent" + agents[i].getId() + " at " + agents[i].getPosition() + ". Gold collected: " + agents[i].getCollectedGold() + ".");
                    }

                    break;
                case MOVE_AGENT:
                    int id  = (int) parsedInput.getArgs()[0];
                    position = new Position((int) parsedInput.getArgs()[1], (int) parsedInput.getArgs()[2]);
                    boolean isMoved = selectedMaze.moveAgent(id,position);
                    if(isMoved)
                        System.out.println("Operation Success.");
                    else
                        System.out.println("Operation Failed.");
                    break;
                case QUIT:
                    System.exit(0);
                    break;
            }
        }
    }
}
